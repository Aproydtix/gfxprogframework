#include "Pacman.h"
#include "logger.h"
#include "Ghost.h"
#include "Audio.hpp"

extern bool pellet_eaten;
extern int tiles[36][28];
extern int g_score;
extern int pellets_left;
extern double timeElapsed;

extern glm::mat4 g_view;
extern glm::vec3 camPos;

extern Ghost ghost1;
extern Ghost ghost2;
extern Ghost ghost3;
extern Ghost ghost4;

void playWaka();

// Unused constructor
Pacman::Pacman() {}

// Default constructor we use
Pacman::Pacman(Material* mat, Mesh* m, Buffer* buff)
{
    material = mat;
    mesh = m;
    buffer = buff;

    glGenVertexArrays(1, &buffer->VAO);
    glBindVertexArray(buffer->VAO);

    glGenBuffers(3, &buffer->VBO[0]);                                           // Generates  3 unique ID for buffer object

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (mesh->vertices.size() * sizeof(glm::vec3)), mesh->vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(0); // <-------------------------------- Remember 

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[1]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (mesh->texture_coordinates.size() * sizeof(glm::vec2)), mesh->texture_coordinates.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[2]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (mesh->normals.size() * sizeof(glm::vec3)), mesh->normals.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(2);


    glGenBuffers(1, &buffer->IBO);                                          // Generates unique ID for Element object
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);                     // Binds unique buffer to a target, GL_ELEMENT_ARRAY_BUFFER here
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->indices.size() * sizeof(glm::uvec3), mesh->indices.data(), GL_STATIC_DRAW);
    // Copies indices data into the GL_ELEMENT_ARRAY_BUFFER target

    // Unbinds everything
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Sets default values
    transform.position = glm::vec3(0.f, 18.f, 0.f);
    startPos = transform.position;
    transform.rotation = glm::mat4(1.0f);
    transform.speed = 4.0f;
    direct = glm::vec3(0.0f);
}

void Pacman::draw()
{
    glm::mat4 model = glm::mat4(1.0f);                              // Identity matrix

    model = glm::translate(model, transform.position);              // Moves pacman from local to world position
    model = model * transform.rotation;                             // Applies rotation


    material->shader->use();                                        // Use the shader bound to the ghost
    material->shader->setMatrix("model", model);                    // Sets the model uniform in the shader program
    material->shader->setMatrix("view", g_view);                    // Sets the view uniform in the shader program
    
    material->shader->setFloat("lightPos", transform.position.x, transform.position.y, 5.0f);   // Sets the light position
    material->shader->setFloat("camPos", camPos.x, camPos.y, camPos.z);    // Sets the camera position


    glBindTexture(GL_TEXTURE_2D, material->texture->getNo());       // Binds the texture
    glBindVertexArray(buffer->VAO);                                 // Bind VAO for drawing
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);             // Bind IBO for drawing

    glDrawElements(GL_TRIANGLES, mesh->indices.size() * sizeof(glm::uvec3), GL_UNSIGNED_INT, 0); // Draws the shape
}

void Pacman::update(double delta_time, char& direction)
{
    // Creates model matrix to translate world directions to model space for rotating pacman in movement direction
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, transform.position);
    model = model * transform.rotation;
    model = glm::inverse(model);


    glm::vec3 dir(0.0f);                    // Wanted direction
    bool collision = false;                 // Collision detection

    // Checks collision based on user direction
    switch (direction)
    {
    // UP
    case 'u':
        dir = glm::vec3(0.0f, 1.0f, 0.0f);  // Sets direction
                                            // Checks collision in the direction
        if (tiles[(int)round(transform.position.y + 0.6)][(int)round(transform.position.x - 0.45)] == 1  || 
            tiles[(int)round(transform.position.y + 0.6)][(int)round(transform.position.x + 0.45)] == 1)
        {   
            collision = true;               // Reverts changes if collision
            dir = glm::vec3(0.0f);
        }
        break;
    // DOWN
    case 'd':
        dir = glm::vec3(0.0f, -1.0f, 0.0f); // Sets direction
                                            // Checks collision in the direction
        if (tiles[(int)round(transform.position.y - 0.6)][(int)round(transform.position.x + 0.45)] == 1 || 
            tiles[(int)round(transform.position.y - 0.6)][(int)round(transform.position.x - 0.45)] == 1)
        {
            collision = true;               // Reverts changes if collision
            dir = glm::vec3(0.0f);
        }
        break;
    // LEFT
    case 'l':
        dir = glm::vec3(-1.0f, 0.0f, 0.0f); // Sets direction
                                            // Checks collision in the direction
        if (tiles[(int)round(transform.position.y + 0.45)][(int) round(transform.position.x - 0.6)] == 1 || 
            tiles[(int)round(transform.position.y - 0.45)][(int)round(transform.position.x - 0.6)] == 1)
        {
            collision = true;               // Reverts changes if collision
            dir = glm::vec3(0.0f);
        }
        break;
    // RIGHT
    case 'r':
        dir = glm::vec3(1.0f, 0.0f, 0.0f);  // Sets direction
                                            // Checks collision in the direction
        if (tiles[(int)round(transform.position.y + 0.45)][(int)round(transform.position.x + 0.6)] == 1 || 
            tiles[(int)round(transform.position.y - 0.45)][(int)round(transform.position.x + 0.6)] == 1)
        {
            collision = true;               // Reverts changes if collision
            dir = glm::vec3(0.0f);
        }
        break;
    }

    if (!collision) direct = dir;           // Sets movement direction if no collision occured
    

    // Collision detection while moving
    int x = direct.x;
    int y = direct.y;

    bool collided = false;

    // RIGHT
    if (x == 1)
    {   // Checks for a wall
        if (tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x + 0.5))] == 1 && 
            tiles[int(round(transform.position.y - 0.45))][int(round(transform.position.x + 0.5))] == 1)
        {
            collided = true;
            direct = glm::vec3(0.0f);
        }
        // Checks for a pellet
        if (tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x))] == 3 && 
            tiles[int(round(transform.position.y - 0.45))][int(round(transform.position.x))] == 3)
        {
            // Sets tile to normal walkable
            tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x + 0.5))] = 0;
            g_score += 10;
            pellets_left--;
            playWaka();     // Plays a random waka sound
        }
        // Checks for a superpellet
        if (tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x))] == 4 && 
            tiles[int(round(transform.position.y - 0.45))][int(round(transform.position.x))] == 4)
        {
            // Sets tile to normal walkable
            tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x + 0.5))] = 0;
            g_score += 50;
            pellets_left--;
            Audio::PlayEffect("scream");
            // Enables scared ghost and eating them
            pellet_eaten = true;
            ghost1.eaten = false;
            ghost2.eaten = false;
            ghost3.eaten = false;
            ghost4.eaten = false;

            // Reverse ghost direction
            ghost1.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost2.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost3.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost4.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);

            // Resets superpellet timer
            timeElapsed = 0;
        }
        // Rotates pacman
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f)), delta_time);
    }

    // LEFT
    if (x == -1)
    {
        // Checks for wall
        if (tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x - 0.5))] == 1 && 
            tiles[int(round(transform.position.y - 0.45))][int(round(transform.position.x - 0.5))] == 1)
        {
            collided = true;
            direct = glm::vec3(0.0f);
        }
        // Checks for pellet
        if (tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x))] == 3 && 
            tiles[int(round(transform.position.y - 0.45))][int(round(transform.position.x))] == 3)
        {
            tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x - 0.5))] = 0;
            g_score += 10;
            pellets_left--;
            playWaka();         // Plays a random waka sound
        }
        // Checks for superpellet
        if (tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x))] == 4 && 
            tiles[int(round(transform.position.y - 0.45))][int(round(transform.position.x))] == 4)
        {
            tiles[int(round(transform.position.y + 0.45))][int(round(transform.position.x - 0.5))] = 0;
            g_score += 50;
            pellets_left--;
            Audio::PlayEffect("scream");
            // Enables scared ghost and eating them
            pellet_eaten = true;
            ghost1.eaten = false;
            ghost2.eaten = false;
            ghost3.eaten = false;
            ghost4.eaten = false;
            // Reverse ghost direction
            ghost1.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost2.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost3.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost4.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            // Resets superpellet timer
            timeElapsed = 0;
        }
        // Rotates pacman
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(0.0f, -1.0f, 0.0f, 0.0f)), delta_time);
    }

    // UP
    if (y == 1)
    {
        // Checks for wall
        if (tiles[int(round(transform.position.y + 0.5))][int(round(transform.position.x - 0.45))] == 1 && 
            tiles[int(round(transform.position.y + 0.5))][int(round(transform.position.x + 0.45))] == 1)
        {
            collided = true;
            direct = glm::vec3(0.0f);
        }
        // Checks for pellet
        if (tiles[int(round(transform.position.y))][int(round(transform.position.x - 0.45))] == 3 && 
            tiles[int(round(transform.position.y))][int(round(transform.position.x + 0.45))] == 3)
        {
            tiles[int(round(transform.position.y + 0.5))][int(round(transform.position.x - 0.45))] = 0;
            g_score += 10;
            pellets_left--;
            playWaka();         // Plays random waka sound
        }
        // Checks for superpellet
        if (tiles[int(round(transform.position.y))][int(round(transform.position.x - 0.45))] == 4 && 
            tiles[int(round(transform.position.y))][int(round(transform.position.x + 0.45))] == 4)
        {
            tiles[int(round(transform.position.y + 0.5))][int(round(transform.position.x - 0.45))] = 0;
            g_score += 50;
            pellets_left--;
            Audio::PlayEffect("scream");
            // Enables scared ghost and eating them
            pellet_eaten = true;
            ghost1.eaten = false;
            ghost2.eaten = false;
            ghost3.eaten = false;
            ghost4.eaten = false;
            // Reverse ghost direction
            ghost1.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost2.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost3.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost4.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            // Resets superpellet timer
            timeElapsed = 0;
        }
        // Rotates pacman
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f)), delta_time);
    }

    // DOWN
    if (y == -1)
    {
        // Checks for wall
        if (tiles[int(round(transform.position.y - 0.5))][int(round(transform.position.x + 0.45))] == 1 && 
            tiles[int(round(transform.position.y - 0.5))][int(round(transform.position.x - 0.45))] == 1)
        {
            collided = true;
            direct = glm::vec3(0.0f);
        }
        // Checks for pellet
        if (tiles[int(round(transform.position.y))][int(round(transform.position.x + 0.45))] == 3 && 
            tiles[int(round(transform.position.y))][int(round(transform.position.x - 0.45))] == 3)
        {            
            tiles[int(round(transform.position.y - 0.5))][int(round(transform.position.x + 0.45))] = 0;
            g_score += 10;
            pellets_left--;
            playWaka();                 // Plays random waka sound
        }
        // Checks for superpellet
        if (tiles[int(round(transform.position.y))][int(round(transform.position.x + 0.45))] == 4 && 
            tiles[int(round(transform.position.y))][int(round(transform.position.x - 0.45))] == 4)
        {
            tiles[int(round(transform.position.y - 0.5))][int(round(transform.position.x + 0.45))] = 0;
            g_score += 50;
            pellets_left--;
            Audio::PlayEffect("scream");
            // Enables scared ghost and eating them
            pellet_eaten = true;
            ghost1.eaten = false;
            ghost2.eaten = false;
            ghost3.eaten = false;
            ghost4.eaten = false;
            // Reverse ghost direction
            ghost1.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost2.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost3.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            ghost4.direct = direct * glm::vec3(-1.0f, -1.0f, 0.0f);
            // Resets superpellet timer
            timeElapsed = 0;
        }
        // Rotates pacman
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f)), delta_time);
    }

    // Resets direction input, and centers pacman on collision
    if (collided)
    {
        direction = ' ';
        transform.position = glm::ivec3(round(transform.position.x), round(transform.position.y), transform.position.z);
    }
 
    // Moves pacman in world space
    transform.translate(direct, delta_time);
    

    // Warp tunnel
    if (transform.position.x > 27) transform.position.x = 0;
    if (transform.position.x < 0) transform.position.x = 27;
}

// Function for playing a random waka sound
void playWaka()
{
    int random = std::rand() % 11;

    switch (random)
    {
    case 0:
        Audio::PlayEffect("1");
        break;

    case 1:
        Audio::PlayEffect("2");
        break;

    case 2:
        Audio::PlayEffect("3");
        break;

    case 3:
        Audio::PlayEffect("4");
        break;

    case 4:
        Audio::PlayEffect("5");
        break;

    case 5:
        Audio::PlayEffect("6");
        break;

    case 6:
        Audio::PlayEffect("7");
        break;

    case 7:
        Audio::PlayEffect("8");
        break;

    case 8:
        Audio::PlayEffect("9");
        break;

    case 9:
        Audio::PlayEffect("10");
        break;

    case 10:
        Audio::PlayEffect("11");
        break;
    }
}