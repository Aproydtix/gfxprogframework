#pragma once
#include "Structs.h"
#include "Entity.h"

class Pellet : public Entity
{
private:
    Mesh* mesh;
    Buffer* buffer;

    int startstate[36][28];
    int startpellets;
public:
    Material* material;

    Pellet();
    Pellet(Material* mat, Mesh* mesh, Buffer* buff, const char* map_path);

    void draw();
    void update(double delta_time);
    void reset();
};