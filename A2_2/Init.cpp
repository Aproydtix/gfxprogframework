﻿#include "Init.h"
#include "logger.h"
#include "mad.h"
#include "Enums.h"
#include "TileMap.h"
#include "Pacman.h"
#include "Pellet.h"
#include "Ghost.h"
#include "Audio.hpp"
#include <vector>

#ifdef _WIN32
#include "Dependencies\glm\glm\glm.hpp"
#include "Dependencies\glm\glm\gtc\matrix_transform.hpp"
#else
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#endif

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

Shader g_shaders[6];
Texture g_textures[6];

Material g_materials[6];
Mesh g_meshes[6];
Buffer g_buffers[6];

TileMap map;
Pellet pellets;
Pacman pacman;
Ghost ghost1;
Ghost ghost2;
Ghost ghost3;
Ghost ghost4;

extern GLFWwindow* g_window;

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

extern glm::mat4 g_projection;
extern glm::mat4 g_view;

bool initWindow()
{
    glfwInit();                                                             // Inits glfw
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);                          // Sets major openGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);                          // Sets minor openGL version
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);          // Sets that we want to use core functionality
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    g_window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Oblig 2", NULL, NULL);          // Creates a window, size 800x600, Learning 2 as title

    if (g_window == NULL)                                                     // If no window created, terminates program
    {
        LOG_ERROR("Error creating window, terminating...\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Window created");

    glfwMakeContextCurrent(g_window);                                       // Sets window to be used
    glewExperimental = GL_TRUE;                                             // For Linux/Mac OS

    if (glewInit() != GLEW_OK)                                              // Inits glew, terminates if not
    {
        LOG_ERROR("Failed to init glew, terminating...\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Glew initiated");

    glEnable(GL_BLEND);                                                     // Enables use of blending
    glEnable(GL_DEPTH_TEST);                                                // Depth test so that only textures on top get rendered
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);                      // Enables the use of alpha channel

    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);                          // Sets the viewport coordinates and size
    glfwSetFramebufferSizeCallback(g_window, windowsize_callback);          // Sets what function to call when screen gets resized
    glClearColor(0.1f, 0.4f, 0.9f, 1.0f);                                   // Sets a clear color / black here
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer
    glfwSwapBuffers(g_window);                                              // Swaps buffer
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer

    LOG_DEBUG("Initialization successful");
    return true;
}

void initShaders()
{
    // Strings for shaderloading
    std::string vertex_string;
    std::string fragment_string;

    // Reads strings from files located in Shaders folder
    mad::readStringFromFile("./Shaders/square.vert", vertex_string);
    mad::readStringFromFile("./Shaders/square.frag", fragment_string);
    // Creates and compiles shader
    g_shaders[walls] = Shader(vertex_string, fragment_string);

    // Reads strings from files located in Shaders folder
    mad::readStringFromFile("./Shaders/pacman.vert", vertex_string);
    mad::readStringFromFile("./Shaders/pacman.frag", fragment_string);
    // Creates and compiles shader
    g_shaders[player] = Shader(vertex_string, fragment_string);

    // Reads strings from files located in Shaders folder
    mad::readStringFromFile("./Shaders/pellet.vert", vertex_string);
    mad::readStringFromFile("./Shaders/pellet.frag", fragment_string);
    // Creates and compiles shader
    g_shaders[consumable] = Shader(vertex_string, fragment_string);

    // Reads strings from files located in Shaders folder
    mad::readStringFromFile("./Shaders/ghost.vert", vertex_string);
    mad::readStringFromFile("./Shaders/ghost.frag", fragment_string);
    // Creates and compiles shader
    g_shaders[ai] = Shader(vertex_string, fragment_string);

    // Reads strings from files located in Shaders folder
    mad::readStringFromFile("./Shaders/text.vert", vertex_string);
    mad::readStringFromFile("./Shaders/text.frag", fragment_string);
    // Creates and compiles shader
    g_shaders[text] = Shader(vertex_string, fragment_string);

    // Sets uniforms for the shaders, equal for all shaders
    g_shaders[walls].setMatrix("projection", g_projection);
    g_shaders[walls].setMatrix("view", g_view);
    g_shaders[consumable].setMatrix("projection", g_projection);
    g_shaders[consumable].setMatrix("view", g_view);
    g_shaders[player].setMatrix("projection", g_projection);
    g_shaders[player].setMatrix("view", g_view);
    g_shaders[ai].setMatrix("projection", g_projection);
    g_shaders[ai].setMatrix("view", g_view);
    g_shaders[text].setMatrix("projection", g_projection);
}

void initTextures()
{
    // Loads textures from files located in Textures folder
    g_textures[walls] = Texture("./Textures/wall.jpg");
    g_textures[player] = Texture("./Textures/smiley.jpg");
}

void initMaterials()
{
    // Runs initShaders and textures before seting the materials to use them
    initShaders();
    initTextures();

    // Sets wall material
    g_materials[walls].shader = &g_shaders[walls];
    g_materials[walls].texture = &g_textures[walls];

    // Sets pellet material
    g_materials[consumable].shader = &g_shaders[consumable];
    g_materials[consumable].texture = &g_textures[1];

    // Sets pacman material
    g_materials[player].shader = &g_shaders[player];
    g_materials[player].texture = &g_textures[player];

    // Sets ghost material
    g_materials[ai].shader = &g_shaders[ai];
    g_materials[ai].texture = &g_textures[1];

    // Sets textbox material
    g_materials[text].shader = &g_shaders[text];
    g_materials[text].texture = &g_textures[text];
}

void initMeshes()
{
    // Default square mesh for ghosts
    std::vector<glm::vec3> vertices = {
        // Front
       glm::vec3( 0.5f,  0.5f,  0.5f),     // Top Right
       glm::vec3( 0.5f, -0.5f,  0.5f),     // Bottom Right
       glm::vec3(-0.5f, -0.5f,  0.5f),     // Bottom Left
       glm::vec3(-0.5f,  0.5f,  0.5f),     // Top Left

       // Right
       glm::vec3( 0.5f,  0.5f, -0.5f),     // Top Right
       glm::vec3( 0.5f, -0.5f, -0.5f),     // Bottom Right
       glm::vec3( 0.5f, -0.5f,  0.5f),     // Bottom Left
       glm::vec3( 0.5f,  0.5f,  0.5f),     // Top Left

        // Back
       glm::vec3( 0.5f,  0.5f, -0.5f),    // Top Right
       glm::vec3( 0.5f, -0.5f, -0.5f),    // Bottom Right
       glm::vec3(-0.5f, -0.5f, -0.5f),    // Bottom Left
       glm::vec3(-0.5f,  0.5f, -0.5f),    // Top Left

         // Left
       glm::vec3(-0.5f,  0.5f,  0.5f),    // Top Right
       glm::vec3(-0.5f, -0.5f,  0.5f),    // Bottom Right
       glm::vec3(-0.5f, -0.5f, -0.5f),    // Bottom Left
       glm::vec3(-0.5f,  0.5f, -0.5f),    // Top Left

         // Top
       glm::vec3( 0.5f,  0.5f, -0.5f),    // Top Right
       glm::vec3( 0.5f,  0.5f,  0.5f),    // Bottom Right
       glm::vec3(-0.5f,  0.5f,  0.5f),    // Bottom Left
       glm::vec3(-0.5f,  0.5f, -0.5f),    // Top Left

         // Bottom
       glm::vec3( 0.5f, -0.5f,  0.5f),    // Top Right
       glm::vec3( 0.5f, -0.5f, -0.5f),    // Bottom Right
       glm::vec3(-0.5f, -0.5f, -0.5f),    // Bottom Left
       glm::vec3(-0.5f, -0.5f,  0.5f),    // Top Left

    };
    g_meshes[ai].vertices = vertices;

    // Default texture coordinates for ghosts
    std::vector<glm::vec2> texCoords = {
        glm::vec2(1.0f, 0.0f),
        glm::vec2(1.0f, 1.0f),
        glm::vec2(0.0f, 1.0f),
        glm::vec2(0.0f, 0.0f),

        glm::vec2(1.0f, 1.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),

        glm::vec2(1.0f, 1.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),

        glm::vec2(1.0f, 1.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),

        glm::vec2(1.0f, 1.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),

        glm::vec2(1.0f, 1.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),
    };
    g_meshes[ai].texture_coordinates = texCoords;

    // Default indices for ghosts
    std::vector<glm::uvec3> indices = {

        glm::uvec3(3, 1, 0),        // Front square
        glm::uvec3(3, 2, 1),

        glm::uvec3(7, 5, 4),        // Right square
        glm::uvec3(7, 6, 5),

        glm::uvec3(11, 9, 8),       // Back square
        glm::uvec3(11, 10, 9),

        glm::uvec3(15, 13, 12),     // Left square
        glm::uvec3(15, 14, 13),

        glm::uvec3(19, 17, 16),     // Top square
        glm::uvec3(19, 18, 17),

        glm::uvec3(23, 21, 20),     // Bottom square
        glm::uvec3(23, 22, 21)
    };
    g_meshes[ai].indices = indices;

    // Defualt normals for ghosts, copied from a blender model
    std::vector<glm::vec3> normals = {
        glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035),
        glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035),
        glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035),
        glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035),

        glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035),
        glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035),
        glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035),
        glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035),

        glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035),
        glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035),
        glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035),
        glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035),

        glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035),
        glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035),
        glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035),
        glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035),

        glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035),
        glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035),
        glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035),
        glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035),

        glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035),
        glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035),
        glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035),
        glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035),
    };
    g_meshes[ai].normals = normals;

    // Loads the pacman sphere from file
    objloadertest(g_meshes[player], "./Models/test.obj");
}

void initEntities()
{
    // Runs the constructor for the different entities for the game
    map = TileMap(&g_materials[walls], &g_meshes[walls], &g_buffers[walls], "./Map/level0.txt");
    pellets = Pellet(&g_materials[consumable], &g_meshes[consumable], &g_buffers[consumable], "./Map/level0.txt");

    pacman = Pacman(&g_materials[player], &g_meshes[player], &g_buffers[player]);

    ghost1 = Ghost(&g_materials[ai], &g_meshes[ai], &g_buffers[ai], glm::vec3(12.0f, 18.0f, 0.0f), 1);
    ghost2 = Ghost(&g_materials[ai], &g_meshes[ai], &g_buffers[ai], glm::vec3(13.0f, 18.0f, 0.0f), 2);
    ghost3 = Ghost(&g_materials[ai], &g_meshes[ai], &g_buffers[ai], glm::vec3(14.0f, 18.0f, 0.0f), 3);
    ghost4 = Ghost(&g_materials[ai], &g_meshes[ai], &g_buffers[ai], glm::vec3(15.0f, 18.0f, 0.0f), 4);
}

void loadSound()
{
    Audio::AddBGM("./Sound/pacman_beginning.wav", "bgm1", false);
    Audio::AddBGM("./Sound/pacman_jazz.wav", "bgm2", false);
    Audio::AddBGM("./Sound/main_menu.wav", "bgm3", false);
    Audio::AddBGM("./Sound/game_over.wav", "bgm4", false);
    Audio::AddBGM("./Sound/victory.wav", "bgm5", false);
    Audio::AddEffect("./Sound/supersayain_scream2.wav", "scream");
    Audio::AddEffect("./Sound/waka1.wav", "1");
    Audio::AddEffect("./Sound/waka2.wav", "2");
    Audio::AddEffect("./Sound/waka3.wav", "3");
    Audio::AddEffect("./Sound/waka4.wav", "4");
    Audio::AddEffect("./Sound/waka5.wav", "5");
    Audio::AddEffect("./Sound/waka6.wav", "6");
    Audio::AddEffect("./Sound/waka7.wav", "7");
    Audio::AddEffect("./Sound/waka8.wav", "8");
    Audio::AddEffect("./Sound/waka9.wav", "9");
    Audio::AddEffect("./Sound/waka10.wav", "10");
    Audio::AddEffect("./Sound/waka11.wav", "11");
    Audio::AddEffect("./Sound/pacman_death.wav", "death");
    Audio::AddEffect("./Sound/pacman_eatghost.wav", "ghost");
}

// Callback function for when the window resizes
void windowsize_callback(GLFWwindow* window, int width, int height)
{

    SCREEN_WIDTH = width;
    SCREEN_HEIGHT = height;

    // Changes projection matrix based on the new width/height ratio
    g_projection = glm::perspective(glm::radians(45.0f), (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);

    // Updates uniforms in the shaders
    g_shaders[walls].setMatrix("projection", g_projection);
    g_shaders[walls].setMatrix("view", g_view);
    g_shaders[consumable].setMatrix("projection", g_projection);
    g_shaders[consumable].setMatrix("view", g_view);
    g_shaders[player].setMatrix("projection", g_projection);
    g_shaders[player].setMatrix("view", g_view);
    g_shaders[ai].setMatrix("projection", g_projection);
    g_shaders[ai].setMatrix("view", g_view);
    g_shaders[text].setMatrix("projection", g_projection);

    glfwGetWindowSize(window, &height, &width);
    glViewport(0, 0, height, width);
}

// Object loader function, original: https://github.com/syoyo/tinyobjloader, changed to fit our needs
void objloadertest(Mesh& mesh, std::string path)
{
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    std::string err;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, path.c_str());

    if (!err.empty())
    {
        LOG_ERROR("%s", err.c_str());
    }
    if (!ret) exit(-1);

    for (size_t s = 0; s < shapes.size(); s++)
    {
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
        {
            int fv = shapes[s].mesh.num_face_vertices[f];

            for (size_t v = 0; v < fv; v++)
            {
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

                // Vertices of the model
                mesh.vertices.push_back(glm::vec3(
                                                attrib.vertices[3 * idx.vertex_index + 0],
                                                attrib.vertices[3 * idx.vertex_index + 1],
                                                attrib.vertices[3 * idx.vertex_index + 2]));

                // Normals of the model
                mesh.normals.push_back(glm::vec3(
                                                attrib.normals[3 * idx.normal_index + 0],
                                                attrib.normals[3 * idx.normal_index + 1],
                                                attrib.normals[3 * idx.normal_index + 2]));
                
                // Texture coordinates of the model
                mesh.texture_coordinates.push_back(glm::vec2(
                                                attrib.texcoords[2 * idx.texcoord_index + 0],
                                                attrib.texcoords[2 * idx.texcoord_index + 1]));

                /////////////////////////////////////////////////////////////////////////////////
                // For some reason this works, no idea why, but atleast it loads the entire model
                // Indices of the model
                mesh.indices.push_back(glm::uvec3(
                                                6 * idx.vertex_index + 0,
                                                6 * idx.vertex_index + 1,
                                                6 * idx.vertex_index + 2));
                mesh.indices.push_back(glm::uvec3(
                                                6 * idx.vertex_index + 3,
                                                6 * idx.vertex_index + 4,
                                                6 * idx.vertex_index + 5));
                /////////////////////////////////////////////////////////////////////////////////
            }
            index_offset += fv;
        }
    }
}
