#include "Ghost.h"
#include "logger.h"
#include "Pacman.h"
#include "Audio.hpp"
#include <time.h>

extern bool pellet_eaten;   // If a superpellet has been eaten
extern bool died;           // if Pacman has died
extern int tiles[36][28];   // The grid
extern int g_lives;

extern int g_score;         
extern glm::mat4 g_view;
extern glm::vec3 camPos;

extern Pacman pacman;   // Necessary for checking collision with pacman

// Unused constructor
Ghost::Ghost() {}

//  Default constructor we use
Ghost::Ghost(Material* mat, Mesh* m, Buffer* buff, glm::vec3 pos, int id)
{
    ID = id;
    material = mat;
    mesh = m;
    buffer = buff;

    elapsedTime = 0;
    eaten = false;

    glGenVertexArrays(1, &buffer->VAO);                                         // Generate VAO with unique ID
    glBindVertexArray(buffer->VAO);                                             // Binds the VAO so changed to buffers will be stored

    glGenBuffers(3, &buffer->VBO[0]);                                           // Generates 3 unique ID for buffer object
    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (mesh->vertices.size() * sizeof(glm::vec3)), mesh->vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(0); // <-------------------------------- Remember 

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[1]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (mesh->texture_coordinates.size() * sizeof(glm::vec2)), mesh->texture_coordinates.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[2]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (mesh->normals.size() * sizeof(glm::vec3)), mesh->normals.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(2);

    glGenBuffers(1, &buffer->IBO);                                          // Generates unique ID for Element object
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);                     // Binds unique buffer to a target, GL_ELEMENT_ARRAY_BUFFER here
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->indices.size() * sizeof(glm::uvec3), mesh->indices.data(), GL_STATIC_DRAW);
    // Copies indices data into the GL_ELEMENT_ARRAY_BUFFER target

    // Unbinds everything
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Set default values
    transform.position = pos;
    startPos = pos;
    transform.speed = 4.0f;
    direct = glm::vec3(1.0f, 0.0f, 0.0f);
    updated = false;
    jailed = true;
}

void Ghost::draw()
{
    glm::mat4 model = glm::mat4(1.0f);                  // Identity matrix

    model = glm::translate(model, transform.position);  // Translates ghost from localspace to worldspace
    model = model * transform.rotation;                 // Applies any rotation

    material->shader->use();                            // Use the shader bound to the ghost
    material->shader->setMatrix("model", model);        // Sets the model uniform in the shader program
    material->shader->setMatrix("view", g_view);        // Sets the view uniform in the shader program

    material->shader->setFloat("lightPos", pacman.transform.position.x, pacman.transform.position.y, 5.0f); // Sets the light position
    material->shader->setFloat("camPos", camPos.x, camPos.y, camPos.z);  // Sets the camera position

    if (pellet_eaten && !eaten)
    {
        material->shader->setInt("ghostID", 5);         // Makes the color darkblue when scared/superpellet eaten
    }
    else
    {
        material->shader->setInt("ghostID", ID);        // Makes the color based on ghostID
    }

    //glBindTexture(GL_TEXTURE_2D, material->texture->getNo()); // Unused texture

    glBindVertexArray(buffer->VAO);                             // Bind VAO for drawing
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);         // Bind IBO for drawing

    glDrawElements(GL_TRIANGLES, mesh->indices.size() * sizeof(glm::uvec3), GL_UNSIGNED_INT, 0); // Draws the shape
}

void Ghost::update(double delta_time)
{
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, transform.position);
    model = model * transform.rotation;
    model = glm::inverse(model);

    // At the start of a round / eaten by pacman
    if (jailed)
    {
        switch (ID)     // Escapes after a set amount of time based on ID
        {
        case 1:
            if (elapsedTime > 0)                // First ghost esacpes at once
            {
                LOG_DEBUG("OUT OF JAIL 1");
                transform.position.y += 3;
                jailed = false;
            }
            break;
        case 2:

            if (elapsedTime > 5)                // Second ghots escapes after 5 seconds
            {
                LOG_DEBUG("OUT OF JAIL 2");
                transform.position.y += 3;
                jailed = false;
            }
            break;
        case 3:
            if (elapsedTime > 10)               // Third ghost escapes after 10 seconds
            {
                LOG_DEBUG("OUT OF JAIL 3");
                transform.position.y += 3;
                jailed = false;
            }
            break;
        case 4:
            if (elapsedTime > 15)               // 4th ghost escapes after 15 seconds
            {
                LOG_DEBUG("OUT OF JAIL 4");
                transform.position.y += 3;
                jailed = false;
            }
            break;
        }
    }

    // Changes speed if scared / superpellet eaten 
    if (pellet_eaten && !eaten)
    {
        transform.speed = 2.0f;
    }
    else
    {
        transform.speed = 4.0f;
    }

    // Checks for a crossroad
    int roads = 0;
    float posx = transform.position.x;
    float posy = transform.position.y;

    // Checks if the tile isn't a wall
    // UP
    if (tiles[int(round(posy + 0.55f))][int(round(posx + 0.45f))] != 1 && tiles[int(round(posy + 0.55f))][int(round(posx - 0.45f))] != 1)
        roads++;
    // RIGHT
    if (tiles[int(round(posy + 0.45f))][int(round(posx + 0.55f))] != 1 && tiles[int(round(posy -0.45f))][int(round(posx + 0.55f))] != 1)
        roads++;
    // DOWN
    if (tiles[int(round(posy - 0.55f))][int(round(posx + 0.45f))] != 1 && tiles[int(round(posy - 0.55f))][int(round(posx - 0.45f))] != 1)
        roads++;
    // LEFT
    if (tiles[int(round(posy + 0.45f))][int(round(posx - 0.55f))] != 1 && tiles[int(round(posy - 0.45f))][int(round(posx - 0.55f))] != 1)
        roads++;

    // if not updated in a while and crossroad, take a random direction
    if (!updated)
    {
        if (roads > 2)
        {
            changeDirection();
            updated = true;
        }
    }
    else
    {
        if (roads < 3)          
        {
            updated = false;
        }
    }

    // Collision detection
    int x = direct.x;
    int y = direct.y;
    bool collided = false;

    // Checks for a walls
    // RIGHT
    if (x == 1)
    {
        if (tiles[(int)round(transform.position.y + 0.45)][(int)round(transform.position.x + 0.5)] == 1 && 
            tiles[(int)round(transform.position.y - 0.45)][(int)round(transform.position.x + 0.5)] == 1)
        {
            collided = true;
        }
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f)), delta_time);
    }
    // LEFT
    if (x == -1)
    {
        if (tiles[(int)round(transform.position.y + 0.45)][(int)round(transform.position.x - 0.5)] == 1 && 
            tiles[(int)round(transform.position.y - 0.45)][(int)round(transform.position.x - 0.5)] == 1)
        {
            collided = true;
        }
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(0.0f, -1.0f, 0.0f, 0.0f)), delta_time);
    }
    // UP
    if (y == 1)
    {
        if (tiles[(int)round(transform.position.y + 0.5)][(int)round(transform.position.x - 0.45)] == 1 && 
            tiles[(int)round(transform.position.y + 0.5)][(int)round(transform.position.x + 0.45)] == 1)
        {
            collided = true;
        }
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f)), delta_time);
    }
    // DOWN
    if (y == -1)
    {
        if (tiles[(int)round(transform.position.y - 0.5)][(int)round(transform.position.x + 0.45)] == 1 && 
            tiles[(int)round(transform.position.y - 0.5)][(int)round(transform.position.x - 0.45)] == 1)
        {
            collided = true;
        }
        transform.rotate(360.0f, glm::vec3(model * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f)), delta_time);
    }

    // Updateds direction if collided and recenters the position
    if (collided)
    {
        updated = false;
        changeDirection();
        transform.position = glm::ivec3(round(transform.position.x), round(transform.position.y), transform.position.z);
    }

    // Circular collision detection with pacman
    x = pow(transform.position.x - pacman.transform.position.x, 2);
    y = pow(transform.position.y - pacman.transform.position.y, 2);

    // If pacman is within 0.5 tiles of the center of the ghost
    if (sqrt(x + y) < 0.5f)
    {
        // If scared / superpellet eaten, ghost gets eaten
        if (pellet_eaten && !eaten)
        {
            Audio::PlayEffect("ghost");
            transform.position = startPos;
            elapsedTime = 0;
            jailed = true;
            eaten = true;
            g_score += 100;
        }
        // Else pacman gets eaten
        else
        {
            g_lives--;
            died = true;
            pellet_eaten = false;   // Ghost can't be eaten twice with the same superpellet, disables the power if it's still up and pacman dies
        }
    }

    // Performs the movement
    transform.translate(direct, delta_time);

    // Warp tunnel
    if (transform.position.x > 27) transform.position.x = 0;
    if (transform.position.x < 0) transform.position.x = 27;

    // Incrementing time
    elapsedTime += delta_time;
}

void Ghost::changeDirection()
{   
    // Very simple AI that changes direction when it hits a wall or randomly at crossroads
    glm::vec3 dir = glm::vec3(0.0f);                // Wanted direction
    int count = 0;                                  // safeguard against endless loop


    while (!updated)                                // Continues until it has found a path
    {
        int random_dir = std::rand() % 4;           // Random direction selector
        switch (random_dir)
        {
        case 0:                                     // Tries to go UP
            if (direct.y != -1.0f || jailed)
            {
                dir = glm::vec3(0.0f, 1.0f, 0.0f);  // Sets direction
                updated = true;                     // Sets to updated
                                                    // If there is a wall in that direction
                if (tiles[(int)round(transform.position.y + 0.6)][(int)round(transform.position.x - 0.45)] == 1 && 
                    tiles[(int)round(transform.position.y + 0.6)][(int)round(transform.position.x + 0.45)] == 1)
                {
                    dir = glm::vec3(0.0f);          // Revert changes
                    updated = false;
                }
            }
            break;
        case 1:                                     // Tries to go RIGHT
            if (direct.x != -1.0f || jailed)
            {
                dir = glm::vec3(1.0f, 0.0f, 0.0f);  // Sets direction
                updated = true;                     // Sets to updated
                if (tiles[(int)round(transform.position.y + 0.45)][(int)round(transform.position.x + 0.6)] == 1 && 
                    tiles[(int)round(transform.position.y - 0.45)][(int)round(transform.position.x + 0.6)] == 1)
                {
                    dir = glm::vec3(0.0f);          // Reverts changes
                    updated = false;
                }
            }
            break;
        case 2:                                     // Tries to go DOWN
            if (direct.y != 1.0f || jailed )
            {
                dir = glm::vec3(0.0f, -1.0f, 0.0f); // Sets direction
                updated = true;                     // Sets to updated
                if (tiles[(int)round(transform.position.y - 0.6)][(int)round(transform.position.x + 0.45)] == 1 && 
                    tiles[(int)round(transform.position.y - 0.6)][(int)round(transform.position.x - 0.45)] == 1)
                {
                    dir = glm::vec3(0.0f);          // Reverts changes
                    updated = false;
                }
            }
            break;
        case 3:                                     // Tries to go LEFT
            if (direct.x != 1.0f || jailed)
            {
                dir = glm::vec3(-1.0f, 0.0f, 0.0f); // Sets direction
                updated = true;                     // Sets to updated
                if (tiles[(int)round(transform.position.y + 0.45)][(int)round(transform.position.x - 0.6)] == 1 && 
                    tiles[(int)round(transform.position.y - 0.45)][(int)round(transform.position.x - 0.6)] == 1)
                {
                    dir = glm::vec3(0.0f);          // Reverts
                    updated = false;
                }
            }
            break;
        }
        count++;                                    // Increments the counter
        if (count > 500) break;                     // Breaks out of loop is too many itterations
    }

    direct = dir;                                   // Sets the new direction
}

void Ghost::reset()
{
    // Resets to default values
    jailed = true;
    elapsedTime = 0;
    transform.position = startPos;
    direct = glm::vec3(1.0f, 0.0f, 0.0f);
}