#include "src/headers/Functions.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Init.h"
#include "src/headers/Globals.h"
#include "src/headers/Camera.h"

#include "src/headers/Movable.h"
#include "src/headers/TextBox.h"

#define AUDIO_IMPLEMENTATION
#include "src/headers/Audio.hpp"

GameObject* cherry;
Mesh* pacman;
Mesh* object;

//--------------------------------------------------------------------
//	USED TO INITIALIZE THE GAME AND YOUR GAMEOBJECTS, CLASSES, AND FUNCTIONS
//--------------------------------------------------------------------
void Start()
{
	initTextures();
	initShaders();
	initMeshes();
	initGameObjects();
    Audio::InitAudio();

	//--------------------------------------------------------------------
	//	TEST CODE, PLEASE DISREGARD
	//--------------------------------------------------------------------
    Audio::AddBGM("../resources/sound/pacman_jazz.wav", "bgm", false);
	object = LoadObject("cherry");
	pacman = LoadObject("Ghost");

	Texture* tex = LoadTexture("cherry");
	Texture* texTest = LoadTexture("test");

	Shader* standardShader = LoadShader("standard");
	Shader* BaWShader = LoadShader("BaW");

    mainCamera = new Camera(glm::vec3(0, 0, 5), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0), freeCam);

	Shader* textShader = LoadShader("text");

	TTF_Font* font = TTF_OpenFont("../resources/fonts/Joystix.TTF", 20);     // Loads the font

	cherry = new GameObject();
	Movable* cherryMovable = new Movable();
	SDL_Color white = { 255,0,255,255 };
	TextBox* text = new TextBox(font, "test", white, glm::vec3(1.0f), left);

	InitGameObject(cherry, object, texTest, standardShader);
	InitGameObject(cherryMovable, object, tex, standardShader);
    InitGameObject(mainCamera, nullptr, nullptr, nullptr);
	InitGameObject(text, text->mesh, text->material.texture, textShader);

	/*for (int i = 0; i < 15; i++)
	InitGameObject(cherry, object, tex, shader);

	InitGameObject((Movable*)cherryMovable, object, tex, shader);
	*/
	/*
	for (int i = 0; i < 15; i++)
	{
		GameObject* cherry = new GameObject();
		cherry->transform.position = glm::vec3((rand() % 5) - 2.5f, (rand() % 5) - 2.5f, (rand() % 5) - 2.5f);
		InitGameObject(cherry, object, tex, standardShader);
	}*/

	Light* light = CreateLight(glm::vec3(1.0f));

    mainCamera->setTarget(cherryMovable);
	mainCamera->setFollowDistance(glm::vec3(0, 5, 5));
	mainCamera->setLookatPosition(glm::vec3(0, 0, 0));

    Audio::PlayBGM("bgm", -1);
}

//--------------------------------------------------------------------
//	USED TO UPDATE THE GAME, OTHERWISE THE SAME AS START
//--------------------------------------------------------------------
void Update()
{



	//--------------------------------------------------------------------
	//	TEST CODE, PLEASE DISREGARD
	//--------------------------------------------------------------------

	lights[0]->transform.position = glm::vec3((rand() % 3) - 1.5f, (rand() % 3) - 1.5f, (rand() % 3) - 1.5f);
	//lights[0]->color = glm::vec3((rand() % 100) / 100.0f, (rand() % 100) / 100.0f, (rand() % 100) / 100.0f);

	for (int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->material.color = glm::vec4((rand() % 100) / 100.0f, (rand() % 100) / 100.0f, (rand() % 100) / 100.0f, 1);

	/*
	GameObject* go = gameObjects[gameObjects.size()-1];
	delete go;
	gameObjects.resize(gameObjects.size() - 1);

	GameObject* cherry = CreateGameObject();
	cherry->transform.position = glm::vec3((rand() % 5) - 2.5f, (rand() % 5) - 2.5f, (rand() % 5) - 2.5f);
	InitGameObject(cherry, object, tex, shader);
	*/

	//cherry->material.shader = BaWShader;
	//cherry->material.texture = texTest;
}
