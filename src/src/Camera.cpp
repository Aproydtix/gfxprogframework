#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

Camera::Camera(glm::vec3 pos, glm::vec3 viewDir, glm::vec3 upDir, CameraMode newMode)
{
    transform.position = pos;
    lookatDirection = viewDir;
    upDirection = upDir;
    mode = newMode;

    rotationAngle = 45.0f;
    rotationSpeed = 1.0f;
    rotationDirection = glm::vec3(0, 1, 0);
    lookatPosition = glm::vec3(0, 0, 0);
    followDistance = glm::vec3(0, 0, 1);
    
    view = glm::mat4(1.0f);
    projection = glm::perspective(glm::radians(45.0f), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 100.0f);
}

void Camera::update()
{
    switch (mode)
    {
    case freeCam:
        // TODO, make moveable
        view = glm::lookAt(transform.position, transform.position + lookatDirection, upDirection);
        view = view * transform.rotation;
        break;

    case staticCam:
        view = glm::lookAt(transform.position, lookatPosition, upDirection);
        break;

    case followCam:
        transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(followDistance, 1.0f));
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case trackingCam:
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case orbitTargetCam:
        // Orbit gameobject
        transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);
        
        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = target->transform.position + followDistance;
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case orbitPositionCam:
        transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);
        
        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = lookatPosition + followDistance;
        view = glm::lookAt(transform.position, lookatPosition, upDirection);
        break;
    }

}

void Camera::input(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
        mode = static_cast<CameraMode>(int(mode) + 1);
        if (int(mode) > 5)
        {
            mode = freeCam;
            transform.position = glm::vec3(0, 0, 5);
            transform.rotation = glm::mat4(1.0f);
        }
        LOG_DEBUG("Cam mode: %d", int(mode));
    }
    if (mode == freeCam)
    {
        glm::vec3 newDir = glm::vec3(0);
        if (key == GLFW_KEY_W)
        {
            transform.position += lookatDirection * 0.25f;
        }
        if (key == GLFW_KEY_S)
        {
            transform.position -= lookatDirection * 0.25f;
        }
        if (key == GLFW_KEY_A)
        {
            glm::vec3 tempDir = glm::rotate(glm::mat4(1.0f), 90.f, glm::vec3(0, -1, 0)) * glm::vec4(lookatDirection * 0.25f, 1.0f);
            transform.position += tempDir;
        }
        if (key == GLFW_KEY_D)
        {
            glm::vec3 tempDir = glm::rotate(glm::mat4(1.0f), 90.f, glm::vec3(0, -1, 0)) * glm::vec4(lookatDirection * 0.25f, 1.0f);
            transform.position -= tempDir;
        }
        if (key == GLFW_KEY_Q)
        {
            glm::vec3 tempDir = glm::rotate(glm::mat4(1.0f), 90.f, glm::vec3(-1, 0, 0)) * glm::vec4(lookatDirection * 0.25f, 1.0f);
            transform.position += tempDir;
        }
        if (key == GLFW_KEY_E)
        {
            glm::vec3 tempDir = glm::rotate(glm::mat4(1.0f), 90.f, glm::vec3(-1, 0, 0)) * glm::vec4(lookatDirection * 0.25f, 1.0f);
            transform.position -= tempDir;
        }
        if (key == GLFW_KEY_R)
        {
            rotate(glm::vec3(-1, 0, 0));
        }
        if (key == GLFW_KEY_F)
        {
            rotate(glm::vec3(1, 0, 0));
        }
        if (key == GLFW_KEY_T)
        {
            rotate(glm::vec3(0, -1, 0));
        }
        if (key == GLFW_KEY_G)
        {
            rotate(glm::vec3(0, 1, 0));
        }
        if (key == GLFW_KEY_Z)
        {
            tilt(glm::vec3(0, 0, -1));
        }
        if (key == GLFW_KEY_X)
        {
            tilt(glm::vec3(0, 0, 1));
        }


        //newDir = transform.rotation * glm::vec4(newDir, 1.0f);
        //transform.position += newDir;
    }
}

void Camera::rotate(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
}

void Camera::tilt(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    upDirection = transform.rotation * glm::vec4(upDirection, 1.0);
}

void Camera::setCamMode(CameraMode newMode)
{
    mode = newMode;
}

void Camera::setFollowDistance(glm::vec3 newDistance)
{
    followDistance = newDistance;
}

void Camera::setUpDirection(glm::vec3 newDirection)
{
    upDirection = newDirection;
}

void Camera::setLookatDirection(glm::vec3 newDirection)
{
    lookatDirection = newDirection;
}

void Camera::setLookatPosition(glm::vec3 newPosition)
{
    lookatPosition = newPosition;
}

void Camera::setTarget(GameObject* newTarget)
{
    target = newTarget;
}

void Camera::setRotationDirection(glm::vec3 newDirection)
{
    rotationDirection = newDirection;
}

void Camera::setRotationAngle(float angle)
{
    rotationAngle = angle;
}

void Camera::setRotationSpeed(float speed)
{
    rotationSpeed = speed;
}

void Camera::updateProjection(int width, int height)
{
    projection = glm::perspective(glm::radians(45.0f), float(width) / float(height), 0.1f, 100.f);
}

glm::vec3 Camera::getFollowDistance()
{
    return followDistance;
}

glm::vec3 Camera::getUpDirection()
{
    return upDirection;
}

glm::vec3 Camera::getLookatDirection()
{
    return lookatDirection;
}

glm::vec3 Camera::getLookatPosition()
{
    return lookatPosition;
}

glm::mat4 Camera::getProjection()
{
    return projection;
}

glm::mat4 Camera::getView()
{
    return view;
}

GameObject* Camera::getTarget()
{
    return target;
}