﻿#pragma once
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

bool initWindow();
void initTextures();
void initShaders();
void initMeshes();
void initGameObjects();
void loadSound();
void inputHandler (GLFWwindow* window, int key, int scancode, int action, int mods);
//void objloadertest(Mesh* mesh, std::string path);

void windowsize_callback(GLFWwindow* window, int width, int height);
