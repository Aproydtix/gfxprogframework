#ifndef FUNCTIONS
#define FUNCTIONS

#include "GL/glew.h"
#include "glfw/glfw3.h"
#include "glm/glm/glm.hpp"
#include "SOIL.h"
#include <string>
#include "enum.h"

bool checkDirection(Direction dir, int x, int y, bool check6);	//	Checks for collision in the given direction from a point, can check for value 6 tiles
void resetGame();												//	Resets the game
void resetLevel();												//	Resets the position of everything in the level
void resetCreatures();											//	Resets creatures
void loadTextures();											//	Loads textures from files
void loadObjs();												//	Loads .obj from files
void loadLevelFromFile(int level);								//	Loads the level from file
void loadText();												//	Load text assets
void initLevel();												//	Initialize the level
void makeWall(int x, int y, int type);							//	Create wall assets
void drawUI();													//	Draws the in-game UI
void drawMenu();												//	Draws the pause menu
void drawMainMenu();                                            //  Draws the main menu
void drawHighScoreMenu();                                       //  Draws the high score menu
void drawLevelSelect();                                         //  Draws level select
void drawText(int x, int y, std::string s);						//	Draws text
void keyboardInput(GLFWwindow* window, int key, int scancode, int action, int mods);	//	Keyboard input
void exitGame();                                                //  Exits to main menu
void quitGame(GLFWwindow* window);								//	Quits the game
int loadHighScore(int lvl);                                     //  Returns high score from given level, 0 if no score
void saveHighScore();                                           //  Saves high score
void setPellets();                                              //  Sets the pellets 
void startGame();                                               //  Starts game from main menu

#endif // !FUNCTIONS