#ifndef PACMAN
#define PACMAN

#include "Creature.h"

class Pacman : public Creature
    {
    public:
		bool dying = false;				//	Whether the player is about to die
    	bool dead = false;				//	Whether the player is dead
    	bool power = false;				//	Wheter the player is under the effect of a power pellet
		Direction nextDir = None;		//	The next movement direction of the player
    
    	Pacman() : Creature(){}			//	Constructor
        Pacman(int x, int y, float s);	//	Constructor
    };
#endif // !PACMAN
