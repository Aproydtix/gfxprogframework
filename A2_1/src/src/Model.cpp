#include "Model.h"
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <string>

using namespace imt2531;
using namespace glm;


Model::Model(imt2531::Shader *sID, imt2531::Texture *tProgramID, std::vector<Object*> obj, glm::vec3 pos, glm::vec3 size, glm::vec3 rot)
{
	//	Set Shader/Texture
	shaderID = sID;
	textureProgramID = tProgramID;
	//	Set objects
	object = obj;

	//	Create sprites and apply transformations
	for (int i = 0; i < object.size(); i++)
	{
		position.push_back(glm::vec3(0,0,0));
		scale.push_back(glm::vec3(0,0,0));
		rotation.push_back(glm::vec3(0,0,0));
		sprite.push_back(new TexVAOVBO(GL_TRIANGLES, object[i]->vertices.size(), object[i]->vertices, object[i]->normals, object[i]->texCoord, shaderID, textureProgramID, GL_FILL));
		setVertices(i, pos, size, rot);
	}
}

void Model::setVertices(int obj, glm::vec3 pos, glm::vec3 size, glm::vec3 rot)
{
	if (position[obj] == pos && scale[obj] == size && rotation[obj] == rot)
		return;

	//	Set transformations
	position[obj] = pos;
	scale[obj] = size;
	rotation[obj] = rot;

	//	Create quaternion from rotation in radians
	quat qrot = quat(rot);

	//	temp vectors
	std::vector <GLfloat> vertices;
	std::vector <GLfloat> normals;

	//	Position, scale, and rotate vertices
	for (int i = 0; i < object[obj]->vertices.size(); i += 3)
	{
		glm::vec3 point = glm::vec3(object[obj]->vertices[i] * scale[obj].x, object[obj]->vertices[i + 1] * scale[obj].y, object[obj]->vertices[i + 2] * scale[obj].z) * qrot;

		vertices.push_back(position[obj].x + point.x);
		vertices.push_back(position[obj].y + point.y);
		vertices.push_back(position[obj].z + point.z);
	}

	//	Position, scale, and rotate normals
	for (int i = 0; i < object[obj]->normals.size(); i += 3)
	{
		glm::vec3 point = glm::vec3(object[obj]->normals[i], object[obj]->normals[i + 1], object[obj]->normals[i + 2]) * qrot;

		normals.push_back(point.x);
		normals.push_back(point.y);
		normals.push_back(point.z);
	}

	//	Update sprite
	sprite[obj]->update(vertices, normals);
}