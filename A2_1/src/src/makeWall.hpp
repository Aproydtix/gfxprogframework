#include "Globals.h"

void makeWall(int x, int y, int type, Texture* tex)
{
	Model model;
	float width = .5f;
	float offset = .125; // this *should* be a formula...
	switch (type)
	{
	case 0:	//middle
		model = Model(ShaderID, tex, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
			glm::vec3(width, width, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	case 1:	//down
		model = Model(ShaderID, tex, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - offset, 0),
			glm::vec3(width, .5 + width / 2, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	case 2: //up
		model = Model(ShaderID, tex, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + offset, 0),
			glm::vec3(width, .5 + width / 2, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	case 3: //down and up
		model = Model(ShaderID, tex, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
			glm::vec3(width, 1, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	case 4: //right
		model = Model(ShaderID, tex, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y, 0),
			glm::vec3(.5 + width / 2, width, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	case 5:	//down and right
		if (levelData[x + 1][y + 1] == 1)
		{   //downRight
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //down
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
				glm::vec3(width, .5 - width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //right
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 + width / 2, width, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 6:	//up and right
		if (levelData[x + 1][y - 1] == 1)
		{   //upRight
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //up
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
				glm::vec3(width, .5 - width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //right
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 + width / 2, width, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 7: //down, up and right
		if (levelData[x + 1][y + 1] != 1)
		{   
			if (levelData[x + 1][y - 1] != 1)
			{   //downUp
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(width, 1, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //right
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(.5 - width / 2, width, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
			else
			{   //upRight
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
					glm::vec3(.5 + width / 2, .5 + width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //down
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
					glm::vec3(width, .5 - width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
		}
		else if (levelData[x + 1][y - 1] != 1)
		{   //downRight
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //up
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
				glm::vec3(width, .5 - width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //downUpRight
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 + width / 2, 1, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 8: //left
		model = Model(ShaderID, tex, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y, 0),
			glm::vec3(.5 + width / 2, width, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	case 9: //down and left
		if (levelData[x - 1][y + 1] == 1)
		{   //downLeft
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //down
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
				glm::vec3(width, .5 - width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //left
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 + width / 2, width, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 10:    //up and left
		if (levelData[x - 1][y - 1] == 1)
		{   //upLeft
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //up
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
				glm::vec3(width, .5 - width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //left
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 + width / 2, width, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 11:    //down, up and left
		if (levelData[x - 1][y + 1] != 1)
		{
			if (levelData[x - 1][y - 1] != 1)
			{   //downUp
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(width, 1, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //left
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(.5 - width / 2, width, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
			else
			{
                //upRight
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
					glm::vec3(.5 + width / 2, .5 + width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //down
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
					glm::vec3(width, .5 - width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
		}
		else if (levelData[x - 1][y - 1] != 1)
		{   //downLeft
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //up
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
				glm::vec3(width, .5 - width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //downUpLeft
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 + width / 2, 1, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 12:    //right and left
		model = Model(ShaderID, tex, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
			glm::vec3(1, width, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	case 13:    //down, left and right
		if (levelData[x - 1][y + 1] != 1)
		{
			if (levelData[x + 1][y + 1] != 1)
			{   //leftRight
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(1, width, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //down
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
					glm::vec3(width, .5 - width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
			else
			{   //downRight
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
					glm::vec3(.5 + width / 2, .5 + width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //left
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(.5 - width / 2, width, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
		}
		else if (levelData[x + 1][y + 1] != 1)
		{   //downLeft
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //right
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 - width / 2, width , 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //downLeftRight
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - offset, 0),
				glm::vec3(1, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 14:    //up, left and right
		if (levelData[x - 1][y - 1] != 1)
		{
			if (levelData[x + 1][y - 1] != 1)
			{   //leftRight
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(1, width, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //up
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
					glm::vec3(width, .5 - width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
			else
			{   //upRight
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
					glm::vec3(.5 + width / 2, .5 + width / 2, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
                //left
				model = Model(ShaderID, tex, cube,
					glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
					glm::vec3(.5 - width / 2, width, 1),
					glm::vec3(0, 0, 0));
				wallModels.push_back(model);
			}
		}
		else if (levelData[x + 1][y - 1] != 1)
		{   //upLeft
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
				glm::vec3(.5 + width / 2, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //right
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(.5 - width / 2, width, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		else
		{   //upLeftRight
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + offset, 0),
				glm::vec3(1, .5 + width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
		}
		break;
	case 15:    //down, up, left and right
	{
		bool ul = (x != 0 && y != 0) ? levelData[(x - 1)][y - 1] == 1 : 0;
		bool ur = (x != levelData.size() - 1 && y != 0) ? levelData[(x + 1)][y - 1] == 1 : 0;
		bool dl = (x != 0 && y != levelData[x].size() - 1) ? levelData[x - 1][(y + 1)] == 1 : 0;
		bool dr = (x != levelData.size() - 1 && y != levelData[x].size() - 1) ? levelData[x + 1][(y + 1)] == 1 : 0;
		int cornerState = ul * 8 + ur * 4 + dl * 2 + dr;
		switch (cornerState)
		{
		case 0: //nothing
            //rightLeft
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(1, width, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //down
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
				glm::vec3(width , .5 - width/ 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
            //up
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
                glm::vec3( width , .5 -width/ 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
			break;
		case 1: //downRight
            //downRight
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //left
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 - width / 2, width, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //up
			model = Model(ShaderID, tex, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
				glm::vec3(width, .5 - width / 2, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
			break;
		case 2: //downLeft
            //downLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //right
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 - width / 2, width, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //up
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
                glm::vec3(width, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 3: //downRightLeft
            //downRightLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(1, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //up
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
                glm::vec3(width, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 4: //upRight
            //upRight
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //down
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
                glm::vec3(width, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //left
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 - width / 2, width, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 5: //downUpRight
            //downUpRight
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 + width / 2, 1, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //left
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 - width / 2, width, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 6: //upRight downLeft
            //upRight
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //left
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 - width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //down
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
                glm::vec3(width, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 7: //downRightLeft upRight
            //downRightLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //right
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 - width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //up
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
                glm::vec3(.5 + width / 2, .5 - width / 2 , 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 8: //upLeft
            //upLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //right
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 - width / 2, width, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //down
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
                glm::vec3(width, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 9: //downRight upLeft
            //downRight
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //left
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 - width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //up
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
                glm::vec3(width, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 10:    //downUpLeft
            //downUpLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 + width / 2, 1, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //right
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(.5 - width / 2, width, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 11:    //downRightLeft upLeft
            //downRightLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //left
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y - offset, 0),
                glm::vec3(.5 - width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //up
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y + (.5 - offset), 0),
                glm::vec3(.5 + width / 2, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 12:    //upRightLeft
            //upRightLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(1, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //down
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
                glm::vec3(width, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 13:    //upRightLeft downRight
            //upRightLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //right
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - (.5 - offset), ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 - width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //down
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + offset, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
                glm::vec3(.5 + width / 2, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 14:    //upRightLeft downLeft
            //upRightLeft
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 + width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //left
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x + (.5 - offset), ((float)(levelData[0].size()) / 2) - y + offset, 0),
                glm::vec3(.5 - width / 2, .5 + width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            //down
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x - offset, ((float)(levelData[0].size()) / 2) - y - (.5 - offset), 0),
                glm::vec3(.5 + width / 2, .5 - width / 2, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
            break;
		case 15:    //everything
            //box
            model = Model(ShaderID, tex, cube,
                glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
                glm::vec3(1, 1, 1),
                glm::vec3(0, 0, 0));
            wallModels.push_back(model);
		default: //should not happen
			model = Model(ShaderID, tYellow, cube,
				glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
				glm::vec3(1, 1, 1),
				glm::vec3(0, 0, 0));
			wallModels.push_back(model);
			break;
		}
	}
	break;
	default:    //should not happen
		model = Model(ShaderID, tYellow, cube,
			glm::vec3(-((float)(levelData.size()) / 2) + x, ((float)(levelData[0].size()) / 2) - y, 0),
			glm::vec3(1, 1, 1),
			glm::vec3(0, 0, 0));
		wallModels.push_back(model);
		break;
	}
}
