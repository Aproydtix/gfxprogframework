#ifndef GLOBALS
#define GLOBALS

#include "GL/glew.h"
#include "glm/glm/glm.hpp"
#include "Model.h"
#include <vector>
#include "pacmanHandler.h"
#include "ghostHandler.h"
#include "pelletHandler.h"
#include "fruitHandler.h"

#define PI 3.14159265358979323846264338327950288	//	Pacman is kind of pie shaped...

extern float pacmanSpeed;	//	Pacman's base speed
extern float ghostSpeed;	//	The ghosts' base speed

extern float phaseTime;		//	Duration of a phase (scatter/chase)
extern float phaseTimer;	//	Timer for phase change
extern float scareTime;		//	Duration of scare
extern float scareTimer;	//	Timer for scare
extern int ghostsEaten;		//	Amount of ghosts eaten during a scare
extern int phaseCount;		//	Amount of phases been through
extern int lives;			//	Pacman's lives
extern bool bonusLife;		//	Whether Pacman has been granted the bonus life
extern bool fruitPlaced[2];	//	Whether the fruit has been placed
extern int fruitEaten;		//	Amount of fruit eaten on the current level

extern float lastFrame;		//	Time of previous frame
extern float deltaTime;		//	Time since last frame
extern float animTimer;		//	Used for global animations	

extern int pelletTotal;		//	Total amount of pellets placed on the level
extern int pelletsActive;	//	Amount of active pellets

extern bool pause;			//	Whether the game is paused or not
extern bool mainMenu;       //  Whether the game is on main menu or not
extern bool levelSelect;    //  Whether the game is on level select or not
extern bool highScoreMenu;  //  Whether the game is on high score menu or not

extern bool windowClosed;   //  Whether the window is closed or not

extern int screenX;			//	The X dimension of the screen
extern int screenY;			//	The Y dimension of the screen
extern int spriteSize;		//	The size of sprites, also a multiplier for scaling the window
extern int textureX;		//	The X dimension of a texture
extern int textureY;		//	The Y dimension of a texture
extern std::vector<std::vector<int>> levelData;	//	A 2D array containing the data for the level

extern int startX;			//	The player's starting position
extern int startY;			//	The player's starting position

extern int level;			//	The current level (not map id, but in-game level)
extern int score;			//	Player score
extern int highScore;		//	Highest score

extern int menuChoice;		//	Current selection in the menu
extern int mainMenuChoice;  //  Current selection in the main menu
extern int selectedLevel;   //  Current selected level

extern int cameraMode;			//	The game's camera mode
extern glm::vec3 cameraFocus;	//	The camera's focus
extern glm::vec3 cameraPosition;//	The camera's position
extern glm::vec3 camOffsets[4];	//	Camera offsets for first-person
extern glm::vec3 currenOffset;	//	Current camera offset

extern PacmanHandler pacmanHandler;		//	Pacman
extern GhostHandler ghostHandler;		//	Ghosts
extern PelletHandler pelletHandler;		//	Pellets
extern FruitHandler fruitHandler;		//	Fruit

//	Objects
extern std::vector<Object*> pacmanObj;	//	Pacman .obj
extern std::vector<Object*> ghostObj;	//	Ghost .obj
extern std::vector<Object*> pelletObj;	//	Pellet .obj
extern std::vector<Object*> fruitObj;	//	All of the fruit .objs

//	Shaders
extern Shader *ShaderID;		//	Shader pointer

//	Textures
extern Texture *tBrickWall;		//	Brick Wall (placeholder, do not use)
extern Texture *tYellow;		//	Yellow texture
extern Texture *tWhite;			//	White texture
extern Texture *tGrey;			//	Grey texture
extern Texture *tBlue;			//	Blue texture
extern Texture *tOranje;		//	Orange texture
extern Texture *tPacMan;		//	Pacman texture
extern Texture *tScaredBlue;	//	Ghost scared blue
extern Texture *tScaredWhite;	//	Ghost scared white
extern Texture *tEyes;			//	Ghost dead (showing eyes only)
extern Texture *tCherry;		//	Fruit
extern Texture *tStrawberry;
extern Texture *tOrange;
extern Texture *tApple;
extern Texture *tMelon;
extern Texture *tGalaBoss;
extern Texture *tBell;
extern Texture *Key;
extern std::vector<Texture*> tFruits;
extern std::vector<Texture*> tGhosts;	//	Ghosts textures
extern std::vector<Texture*> tAscii;	//	Text textures

//	Wallmodel
extern std::vector<Model> wallModels;	//	WallModels
extern std::vector<Model> ascii;		//	Text"Models"
extern std::vector<Model> uiLife;		//	Life Icons
extern std::vector<Model> uiFruit;		//	Fruit Icons

//	A cube
extern std::vector<GLfloat> cubeVertices;	//	Cube Vertices
extern std::vector<GLfloat> cubeNormals;	//	Cube Normals
extern std::vector<GLfloat> cubeTextCoord;	//	Cube texture coordinates
extern std::vector<Object*> cube;			//	Cube object

#endif // !GLOBALS