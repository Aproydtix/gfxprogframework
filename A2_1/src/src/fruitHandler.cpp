#include "fruitHandler.h"
#include "Globals.h"
#include "Audio.h"

void FruitHandler::Init()
{
	fruit = Fruit(13, 20);						//	Create object
	Model model(ShaderID, tCherry, fruitObj,	//	Create model
		glm::vec3(0, 0, 0),						//	pos
		glm::vec3(0.4f, 0.4f, 0.4f),			//	scale
		glm::vec3(0, 0, 0));					//	rot
	fruit.model.push_back(model);				//	Add to models
}

void FruitHandler::update()
{
	if (fruit.active)					//	If fruit is active
	{
		fruit.timer -= deltaTime;		//	Reduce timer
		if (fruit.timer <= 0)			//	If less than 0
			fruit.active = false;		//	Fruit is no longer active

		fruit.sprite = (level) / 2;		//	Fruit type based on level
		if (fruit.sprite > tFruits.size()-1)			//
			fruit.sprite = tFruits.size()-1;			//	Max fruit type

		draw();							//	Draw fruit

		if (sqrtf	//	Check distance to Pacman
		(((pacmanHandler.pacman.posX + pacmanHandler.pacman.moveX) - (fruit.posX + .5f)) * ((pacmanHandler.pacman.posX + pacmanHandler.pacman.moveX) - (fruit.posX + .5f))
			+ ((pacmanHandler.pacman.posY + pacmanHandler.pacman.moveY) - (fruit.posY)) * ((pacmanHandler.pacman.posY + pacmanHandler.pacman.moveY) - (fruit.posY))
		) < .5f)
		{
			eat();	//	If less than .5 units, eat fruit
		}
	}

	if (pelletTotal - 70 >= pelletsActive)	//	If 70 pellets have been eaten
	{
		if (fruitPlaced[0] == false)
		{
			fruitPlaced[0] = true;			//	Place fruit
			fruit.active = true;			//	Fruit is active
			fruit.timer = 9.5f;				//	Set Timer
		}
	}

	if (pelletTotal - 170 >= pelletsActive)	  //	If 170 pellets have been eaten
	{
		if (fruitPlaced[1] == false)
		{
			fruitPlaced[1] = true;			  //	Place fruit
			fruit.active = true;			  //	Fruit is active
			fruit.timer = 9.5f;				  //	Set Timer
		}
	}
}

void FruitHandler::draw()			//	Draw Fruit
{
	fruit.model[0].sprite[fruit.sprite]->setTexture(tFruits[fruit.sprite]);
	fruit.model[0].setVertices(
		fruit.sprite,
		glm::vec3(-((float)(levelData.size()) / 2) + (fruit.posX + .5f), ((float)(levelData[0].size()) / 2) - fruit.posY, 0),
		glm::vec3(1, 1, 1)*.67f,
		glm::vec3(0, 0, 0));
	fruit.model[0].sprite[fruit.sprite]->setCamera(cameraFocus, cameraPosition);
	fruit.model[0].sprite[fruit.sprite]->draw();
}

void FruitHandler::reset()			//	Reset
{
	fruit.posX = 13; 
	fruit.posY = 20;
	fruit.active = false;
}

void FruitHandler::eat()			//	Eat Fruit
{
	PlayEffect("pacman_eatfruit");	//	Play sound effect
	int bonus = 100;				
	if (fruit.sprite == 1)
		bonus = 300;
	if (fruit.sprite == 2)
		bonus = 500;
	if (fruit.sprite == 3)
		bonus = 700;
	if (fruit.sprite == 4)
		bonus = 1000;
	if (fruit.sprite == 5)
		bonus = 2000;
	if (fruit.sprite == 6)
		bonus = 3000;
	if (fruit.sprite == 7)
		bonus = 5000;

	score += bonus;					//	Increase score
	fruitEaten++;					//	More fruits have been eaten
	fruit.active = false;			//	Set fruit to false
}