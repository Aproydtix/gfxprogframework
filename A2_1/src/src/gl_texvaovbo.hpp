#ifndef vaos
#define vaos

#include "gl_texture.hpp"
#include "gl_shader.hpp"

namespace imt2531
{
	class TexVAOVBO :public VAO
	{
	public:
		Shader * ShaderID;
		Texture *textureProgramID;

		glm::vec3 cameraFocus;
		glm::vec3 cameraPosition;

		GLuint id() const { return VertexArrayID; }
		Shader getVshader() const { return *ShaderID; }
		Texture getTexture() const { return *textureProgramID; }

		TexVAOVBO(GLenum primitive_mode, int numVertices, std::vector<GLfloat> vertices, std::vector<GLfloat> normal, std::vector<GLfloat> texture,
			Shader *vertexProgram, Texture *textureProgram, GLenum fill_mode = GL_FILL)
		{
			//vertices, normal, color, texture,
			/*-----------------------------------------------------------------------------
			*  Generate Vertex and Bind it
			*-----------------------------------------------------------------------------*/
			this->ShaderID = vertexProgram;
			this->textureProgramID = textureProgram;
			this->NumVertices = numVertices / 3;
			this->PrimitiveMode = primitive_mode;

			this->FillMode = fill_mode;

			// Create Vertex Array Object
			// Should be done after CreateWindow and before any other GL calls
			glGenVertexArrays(1, &(this->VertexArrayID)); // VAO
			glBindVertexArray(this->VertexArrayID); // Bind the VAO

			glGenBuffers(1, &(this->VertexBuffer)); // VBO - vertices
			glBindBuffer(GL_ARRAY_BUFFER, this->VertexBuffer); // Bind the VBO vertices
			glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW); // Copy the vertices into VBO
			GLint positionID = glGetAttribLocation(this->ShaderID->id(), "position");
			glVertexAttribPointer(positionID, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(positionID);

			glGenBuffers(1, &(this->NormalBuffer));  // VBO - Normal
			glBindBuffer(GL_ARRAY_BUFFER, this->NormalBuffer); // Bind the VBO vertices
			glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(GLfloat), &normal[0], GL_STATIC_DRAW); // Copy the vertices into VBO
			GLint normalID = glGetAttribLocation(this->ShaderID->id(), "normal");
			glVertexAttribPointer(normalID, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(normalID);   //<-- Enable normalID attribute

			glGenBuffers(1, &(this->TextureBuffer));  // VBO - textures
			glBindBuffer(GL_ARRAY_BUFFER, this->TextureBuffer); // Bind the VBO textures
			glBufferData(GL_ARRAY_BUFFER, (numVertices / 3) * 2 * sizeof(GLfloat), &texture[0], GL_STATIC_DRAW);  // Copy the vertex into VBO
			GLint textureCoordinateID = glGetAttribLocation(this->ShaderID->id(), "textureCoordinate");
			glVertexAttribPointer(textureCoordinateID, 2, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(textureCoordinateID);
		}

		void bind() 
		{
			glBindBuffer(GL_ARRAY_BUFFER, this->VertexBuffer); // Bind the VBO vertices 
		}

		void unbind() { glBindVertexArray(0); }

		void setTexture(Texture* tex)
		{
			textureProgramID = tex;
		}

		void update(std::vector<GLfloat> vertices, std::vector<GLfloat> normals)
		{
			bind();
			/*-----------------------------------------------------------------------------
			*  Load data onto GPU
			*-----------------------------------------------------------------------------*/

			glBufferData(GL_ARRAY_BUFFER, NumVertices * 3 * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW); // Copy the vertices into VBO

            glBindBuffer(GL_ARRAY_BUFFER, this->NormalBuffer); // Bind the VBO vertices
            glBufferData(GL_ARRAY_BUFFER, NumVertices * 3 * sizeof(GLfloat), &normals[0], GL_STATIC_DRAW);

			//unbind();
		}

		void setCamera(glm::vec3 focus, glm::vec3 position)
		{
			cameraFocus = focus;
			cameraPosition = position;
		}

		void draw()
		{
			static float time = 0.0;
			time += .00005;
			glUseProgram(this->textureProgramID->id());

			glBindTexture(GL_TEXTURE_2D, this->textureProgramID->id());
			glBindVertexArray(this->id());

			GLint modelID = glGetUniformLocation(this->ShaderID->id(), "model");
			GLint viewID = glGetUniformLocation(this->ShaderID->id(), "view");
			GLint projectionID = glGetUniformLocation(this->ShaderID->id(), "projection");
			GLint normalMatrixID = glGetUniformLocation(this->ShaderID->id(), "normalMatrix");

			glm::mat4 view = glm::lookAt(cameraPosition, cameraFocus, glm::vec3(0, 0, 1));
			glm::mat4 proj = glm::perspective(3.14f / 3.0f, (GLfloat)1024 / (GLfloat)768, 0.1f, -100.0f);
			//glm::mat4 model = glm::rotate(glm::mat4(), time, glm::vec3(0, 1, 0));
			glm::mat4 model = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0, 0, 1));
			glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(view*model)));

			glUniformMatrix4fv(viewID, 1, GL_FALSE, glm::value_ptr(view));
			glUniformMatrix4fv(projectionID, 1, GL_FALSE, glm::value_ptr(proj));
			glUniformMatrix4fv(modelID, 1, GL_FALSE, glm::value_ptr(model));
			glUniformMatrix3fv(normalMatrixID, 1, GL_FALSE, glm::value_ptr(normalMatrix));

			glDrawArrays(this->PrimitiveMode, 0, this->NumVertices);
		}
	};
}
#endif