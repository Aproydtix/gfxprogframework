#include "Globals.h"

//	See header for info
float pacmanSpeed = .1f;
float ghostSpeed = .09f;

float phaseTime = 7;
float phaseTimer = 0;
float scareTime = 10;
float scareTimer = 0;
int ghostsEaten = 0;
int phaseCount = 0;
int lives = 2;
bool bonusLife = false;
bool fruitPlaced[2]{false,false};
int fruitEaten = 0;

float lastFrame;
float deltaTime;
float animTimer = 0;

int pelletTotal;
int pelletsActive;

bool pause = false;
bool mainMenu = true;
bool levelSelect = false;
bool highScoreMenu = false;

bool windowClosed = false;

int screenX;
int screenY;
int spriteSize = 16;
int textureX;
int textureY;
std::vector<std::vector<int>> levelData;

int startX = 13;
int startY = 26;

int level = 1;
int score = 0;
int highScore = 0;

int menuChoice = 0;
int mainMenuChoice = 0;
int selectedLevel = 0;

//	Camera
int cameraMode = 0;
glm::vec3 cameraFocus;
glm::vec3 cameraPosition;
glm::vec3 camOffsets[4]{
	glm::vec3(-4,0,3),
	glm::vec3(4,0,3),
	glm::vec3(0,-4,3),
	glm::vec3(0,4, 3)
};
glm::vec3 currenOffset;

//	Handlers
PacmanHandler pacmanHandler;		
GhostHandler ghostHandler;		
PelletHandler pelletHandler;		
FruitHandler fruitHandler;		

//	Objects
std::vector<Object*> pacmanObj;
std::vector<Object*> ghostObj;
std::vector<Object*> pelletObj;
std::vector<Object*> fruitObj;

//	Shader
Shader *ShaderID;

//	Textures
Texture *tBrickWall;
Texture *tYellow;
Texture *tWhite;
Texture *tGrey;
Texture *tBlue;
Texture *tOranje;
Texture *tPacMan;
Texture *tScaredBlue;
Texture *tScaredWhite;
Texture *tEyes;
Texture *tCherry;
Texture *tStrawberry;
Texture *tOrange;
Texture *tApple;
Texture *tMelon;
Texture *tGalaBoss;
Texture *tBell;
Texture *Key;
std::vector<Texture*> tFruits;
std::vector<Texture*> tGhosts;
std::vector<Texture*> tAscii;

std::vector<Model> wallModels;
std::vector<Model> ascii;
std::vector<Model> uiLife;
std::vector<Model> uiFruit;

std::vector<GLfloat> cubeVertices = {
	-0.5f, -0.5f, -0.5f,
	+0.5f, -0.5f, -0.5f,
	+0.5f, +0.5f, -0.5f,
	+0.5f, +0.5f, -0.5f,
	-0.5f, +0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, -0.5f, +0.5f,
	+0.5f, -0.5f, +0.5f,
	+0.5f, +0.5f, +0.5f,
	+0.5f, +0.5f, +0.5f,
	-0.5f, +0.5f, +0.5f,
	-0.5f, -0.5f, +0.5f,

	-0.5f, +0.5f, +0.5f,
	-0.5f, +0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, +0.5f,
	-0.5f, +0.5f, +0.5f,

	+0.5f, +0.5f, +0.5f,
	+0.5f, +0.5f, -0.5f,
	+0.5f, -0.5f, -0.5f,
	+0.5f, -0.5f, -0.5f,
	+0.5f, -0.5f, +0.5f,
	+0.5f, +0.5f, +0.5f,

	-0.5f, -0.5f, -0.5f,
	+0.5f, -0.5f, -0.5f,
	+0.5f, -0.5f, +0.5f,
	+0.5f, -0.5f, +0.5f,
	-0.5f, -0.5f, +0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, +0.5f, -0.5f,
	+0.5f, +0.5f, -0.5f,
	+0.5f, +0.5f, +0.5f,
	+0.5f, +0.5f, +0.5f,
	-0.5f, +0.5f, +0.5f,
	-0.5f, +0.5f, -0.5f,
};
std::vector<GLfloat> cubeNormals = {
	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f,  0.5f, -0.5f,
	0.5f,  0.5f, -0.5f,
	-0.5f,  0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f, -0.5f,  0.5f,
	0.5f, -0.5f,  0.5f,
	0.5f,  0.5f,  0.5f,
	0.5f,  0.5f,  0.5f,
	-0.5f,  0.5f,  0.5f,
	-0.5f, -0.5f,  0.5f,

	-0.5f,  0.5f,  0.5f,
	-0.5f,  0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f,  0.5f,
	-0.5f,  0.5f,  0.5f,

	0.5f,  0.5f,  0.5f,
	0.5f,  0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f,  0.5f,
	0.5f,  0.5f,  0.5f,

	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f, -0.5f,  0.5f,
	0.5f, -0.5f,  0.5f,
	-0.5f, -0.5f,  0.5f,
	-0.5f, -0.5f, -0.5f,

	-0.5f,  0.5f, -0.5f,
	0.5f,  0.5f, -0.5f,
	0.5f,  0.5f,  0.5f,
	0.5f,  0.5f,  0.5f,
	-0.5f,  0.5f,  0.5f,
	-0.5f,  0.5f, -0.5f,
};
std::vector<GLfloat> cubeTextCoord = {
	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,
	0.0f,  0.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,
	0.0f,  0.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,
	0.0f,  0.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,
	0.0f,  0.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,
	0.0f,  0.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,
	0.0f,  0.0f,
};
std::vector<Object*> cube; 
